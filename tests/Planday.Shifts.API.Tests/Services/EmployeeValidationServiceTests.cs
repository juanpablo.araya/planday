﻿using System.Linq;
using System.Threading.Tasks;
using Moq;
using Moq.AutoMock;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services;
using Shouldly;
using Xunit;

namespace Planday.Shifts.API.Tests.Services
{
    public abstract class EmployeeAddUpdateValidationTests
    {
        protected abstract Task<OperationResult> ExecuteAction(EmployeeValidationService employeeValidationService,
            Employee employee);

        [Fact]
        public virtual async Task EmailAlreadyExists_ShouldShowError()
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var employee = GetValidEmployee();
            employeeRepositoryMock
                .Setup(repository => repository.FindByEmail(employee.Email))
                .ReturnsAsync(new Employee());

            // Act
            var result = await ExecuteAction(validationService, employee);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Employee.Email))
                .Any(error => error.Value == Constants.EmployeeValidationErrors.EmailAlreadyExists)
                .ShouldBeTrue();
            employeeRepositoryMock.Verify(repository => repository.FindByEmail(employee.Email), Times.Once);
        }

        [Theory]
        [InlineData("         ")]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("anIncorrect")]
        [InlineData("anIncorrect@")]
        public async Task InvalidEmail_ShouldNotValidateIfExists_ShouldShowError(string email)
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var employee = GetValidEmployee();
            employee.Email = email;

            // Act
            var result = await ExecuteAction(validationService, employee);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Employee.Email))
                .Any(error => error.Value == Constants.EmployeeValidationErrors.InvalidEmail)
                .ShouldBeTrue();
            employeeRepositoryMock.Verify(repository => repository.FindByEmail(employee.Email), Times.Never);
        }

        [Theory]
        [InlineData("         ")]
        [InlineData(null)]
        [InlineData("")]
        public async Task InvalidFirstName_ShouldShowError(string firstName)
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employee = GetValidEmployee();
            employee.FirstName = firstName;

            // Act
            var result = await ExecuteAction(validationService, employee);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Employee.FirstName))
                .Any(error => error.Value == Constants.EmployeeValidationErrors.InvalidFirstName)
                .ShouldBeTrue();
        }

        [Theory]
        [InlineData("         ")]
        [InlineData(null)]
        [InlineData("")]
        public async Task InvalidLastName_ShouldShowError(string lastName)
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employee = GetValidEmployee();
            employee.LastName = lastName;

            // Act
            var result = await ExecuteAction(validationService, employee);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Employee.LastName))
                .Any(error => error.Value == Constants.EmployeeValidationErrors.InvalidLastName)
                .ShouldBeTrue();
        }

        [Fact]
        public void IsEmployeeValid_EmployeeIsValid_ShouldReturnOK()
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employee = GetValidEmployee();

            // Act
            var result = validationService.IsEmployeeValid(employee);

            // Assert
            result.Success.ShouldBeTrue();
        }

        [Fact]
        public void IsEmployeeValid_EmployeeIsInvalid_ShouldReturnErrors()
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employee = new Employee();

            // Act
            var result = validationService.IsEmployeeValid(employee);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors.Any(error => error.Property == nameof(Employee.FirstName)).ShouldBeTrue();
            result.ValidationErrors.Any(error => error.Property == nameof(Employee.LastName)).ShouldBeTrue();
            result.ValidationErrors.Any(error => error.Property == nameof(Employee.Email)).ShouldBeTrue();
        }

        protected virtual Employee GetValidEmployee()
        {
            return new Employee
            {
                Id = 0,
                FirstName = "The firstname",
                LastName = "The lastname",
                Email = "the@mail.com",
                Deleted = false
            };
        }
    }

    public class EmployeeAddValidationServiceTests : EmployeeAddUpdateValidationTests
    {
        [Fact]
        public async Task UserIsValid_ShouldReturnIsOK()
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var employee = GetValidEmployee();
            employeeRepositoryMock
                .Setup(repository => repository.FindByEmail(employee.Email))
                .ReturnsAsync((Employee) null);

            // Act
            var result = await ExecuteAction(validationService, employee);

            // Assert
            result.Success.ShouldBeTrue();
            employeeRepositoryMock.Verify(repository => repository.FindByEmail(employee.Email), Times.Once);
        }

        protected override Task<OperationResult> ExecuteAction(EmployeeValidationService employeeValidationService,
            Employee employee)
        {
            return employeeValidationService.IsValidForAdd(employee);
        }
    }

    public class EmployeeUpdateValidationServiceTests : EmployeeAddUpdateValidationTests
    {
        [Fact]
        public async Task UserIsValid_ShouldReturnIsOK()
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var employee = GetValidEmployee();
            employeeRepositoryMock
                .Setup(repository => repository.FindByEmail(employee.Email))
                .ReturnsAsync(employee);

            // Act
            var result = await ExecuteAction(validationService, employee);

            // Assert
            result.Success.ShouldBeTrue();
            employeeRepositoryMock.Verify(repository => repository.FindByEmail(employee.Email), Times.Once);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task UserIdIsInValid_ShouldShowError(int employeeId)
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            var employee = GetValidEmployee();
            employee.Id = employeeId;

            // Act
            var result = await ExecuteAction(validationService, employee);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Employee.Id))
                .Any(error => error.Value == Constants.EmployeeValidationErrors.InvalidId)
                .ShouldBeTrue();
        }

        // TODO: validate when checking the user by email and the repository returns null

        protected override Task<OperationResult> ExecuteAction(EmployeeValidationService employeeValidationService,
            Employee employee)
        {
            return employeeValidationService.IsValidForUpdate(employee);
        }

        protected override Employee GetValidEmployee()
        {
            var employee = base.GetValidEmployee();
            employee.Id = 123;
            return employee;
        }
    }

    public class EmployeeDeleteValidationServiceTests
    {
        [Fact]
        public void ValidId_ShouldReturnOK()
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();
            const int employeeId = 1;

            // Act
            var result = validationService.IsValidForDelete(employeeId);

            // Assert
            result.Success.ShouldBeTrue();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void InValidId_ShouldShowError(int employeeId)
        {
            // Arrange
            var mocker = new AutoMocker();
            var validationService = mocker.CreateInstance<EmployeeValidationService>();

            // Act
            var result = validationService.IsValidForDelete(employeeId);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Employee.Id))
                .Any(error => error.Value == Constants.EmployeeValidationErrors.InvalidId)
                .ShouldBeTrue();
        }
    }
}