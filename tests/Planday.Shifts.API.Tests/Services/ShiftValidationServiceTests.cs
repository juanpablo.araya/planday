﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Moq.AutoMock;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services;
using Planday.Shifts.API.Services.Abstractions;
using Shouldly;
using Xunit;

namespace Planday.Shifts.API.Tests.Services
{
    public abstract class AddUpdateShiftValidationServiceTests
    {
        protected readonly ShiftValidationService ShiftValidationService;
        private readonly Mock<IEmployeeRepository> _employeeRepositoryMock;
        protected readonly Mock<IShiftRepository> ShiftRepositoryMock;

        protected AddUpdateShiftValidationServiceTests()
        {
            var mocker = new AutoMocker();
            ShiftValidationService = mocker.CreateInstance<ShiftValidationService>();
            _employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            ShiftRepositoryMock = mocker.GetMock<IShiftRepository>();
        }

        protected abstract Task<OperationResult> ExecuteAction(ShiftValidationService shiftValidationService,
            Shift shift);

        [Fact]
        public async Task Add_ShiftIsCorrect_ShouldReturnIsOK()
        {
            // Arrange
            var shift = GetValidShift();

            // Act
            var result = await ExecuteAction(ShiftValidationService, shift);

            // Assert
            result.Success.ShouldBeTrue();
            _employeeRepositoryMock.Verify(repository => repository.Get(shift.EmployeeId));
            ShiftRepositoryMock.Verify(repository => repository.GetByEmployeeId(shift.EmployeeId));
        }

        [Fact]
        public async Task Add_ShiftDatesAreInCorrect_ShouldShowError()
        {
            // Arrange
            var shift = GetValidShift();
            // Shift ends on a previous date than start
            var fromDate = shift.From;
            shift.From = shift.To;
            shift.To = fromDate;

            // Act
            var result = await ExecuteAction(ShiftValidationService, shift);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Shift.To))
                .Any(error => error.Value == Constants.ShiftValidationErrors.ToDateStartsBeforeFromDate)
                .ShouldBeTrue();
        }

        [Fact]
        public async Task Add_EmployeeDoesNotExist_ShouldShowError()
        {
            // Arrange
            var shift = GetValidShift();
            shift.EmployeeId += 1; // just a different Id than the configured in the repository mock

            // Act
            var result = await ExecuteAction(ShiftValidationService, shift);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Shift.EmployeeId))
                .Any(error => error.Value == Constants.ShiftValidationErrors.EmployeeDoesNotExist)
                .ShouldBeTrue();
        }

        [Theory]
        [InlineData(22, 12, 2020, 8, 45, 22, 12, 2020, 11, 0)]
        [InlineData(22, 12, 2020, 14, 0, 22, 12, 2020, 19, 0)]
        [InlineData(21, 12, 2020, 18, 0, 21, 12, 2020, 23, 55)]
        [InlineData(21, 12, 2020, 23, 0, 22, 12, 2020, 18, 0)]
        public async Task Add_EmployeeAlreadyHasAShiftInThatRange_ShouldShowError(
            int fromDay,
            int fromMonth,
            int fromYear,
            int fromHour,
            int fromMinute,
            int toDay,
            int toMonth,
            int toYear,
            int toHour,
            int toMinute
        )
        {
            // Arrange
            var shift = GetValidShift();
            // Existing shifts:
            // 21/12/2020 23:50 - 22/12/2020 08:10
            // 22/12/2020 10:00 - 15:00
            // Shifts to add:
            //  22/12/2020 08:45 - 11:00: matches second shift
            //  22/12/2020 14:00 - 19:00: matches second shift
            //  21/12/2020 18:00 - 23:55: matches first shift
            //  21/12/2020 23:00 - 22/12/2020 18:00: matches both shifts
            var employeeShifts = new List<Shift>
            {
                new Shift
                {
                    Id = 1,
                    EmployeeId = shift.EmployeeId,
                    From = new DateTime(2020, 12, 21).AddHours(23).AddMinutes(50),
                    To = new DateTime(2020, 12, 22).AddHours(8).AddMinutes(10)
                },
                new Shift
                {
                    Id = 2,
                    EmployeeId = shift.EmployeeId,
                    From = new DateTime(2020, 12, 22).AddHours(10),
                    To = new DateTime(2020, 12, 22).AddHours(15)
                }
            };
            ShiftRepositoryMock
                .Setup(repository => repository.GetByEmployeeId(shift.EmployeeId))
                .ReturnsAsync(employeeShifts);
            shift.From = new DateTime(fromYear, fromMonth, fromDay).AddHours(fromHour).AddMinutes(fromMinute);
            shift.To = new DateTime(toYear, toMonth, toDay).AddHours(toHour).AddMinutes(toMinute);

            // Act
            var result = await ExecuteAction(ShiftValidationService, shift);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Shift))
                .Any(error => error.Value == Constants.ShiftValidationErrors.EmployeeAlreadyHasShiftsAtThatRange)
                .ShouldBeTrue();
        }

        protected Shift GetValidShift()
        {
            const int employeeId = 1;
            _employeeRepositoryMock.Setup(repository => repository.Get(employeeId))
                .ReturnsAsync(new Employee {Id = employeeId});
            ShiftRepositoryMock.Setup(repository => repository.GetByEmployeeId(employeeId))
                .ReturnsAsync(new List<Shift>());
            var shift = new Shift
            {
                Id = 10,
                EmployeeId = employeeId,
                From = new DateTime(2020, 10, 11).AddHours(10).AddMinutes(15),
                To = new DateTime(2020, 10, 11).AddHours(15).AddMinutes(15)
            };
            ShiftRepositoryMock.Setup(repository => repository.Get(shift.Id))
                .ReturnsAsync(shift);
            return shift;
        }
    }

    public class AddShiftValidationServiceTests : AddUpdateShiftValidationServiceTests
    {
        protected override Task<OperationResult> ExecuteAction(ShiftValidationService shiftValidationService,
            Shift shift)
        {
            return shiftValidationService.IsvalidForAdd(shift);
        }
    }

    public class UpdateShiftValidationServiceTests : AddUpdateShiftValidationServiceTests
    {
        protected override Task<OperationResult> ExecuteAction(ShiftValidationService shiftValidationService,
            Shift shift)
        {
            return shiftValidationService.IsvalidForUpdate(shift);
        }

        [Fact]
        public async Task ShiftIdDoesNotExist_ShouldShowError()
        {
            // Arrange
            var shift = GetValidShift();
            shift.Id = 100;
            ShiftRepositoryMock
                .Setup(repository => repository.Get(shift.Id))
                .ReturnsAsync((Shift) null);

            // Act
            var result = await ExecuteAction(ShiftValidationService, shift);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(Shift.Id))
                .Any(error => error.Value == Constants.ShiftValidationErrors.ShiftDoesNotExist)
                .ShouldBeTrue();
            ShiftRepositoryMock.Verify(repository => repository.Get(shift.Id));
        }
    }

    public class DeleteShiftTests
    {
        private readonly ShiftValidationService _shiftValidationService;

        public DeleteShiftTests()
        {
            var mocker = new AutoMocker();
            _shiftValidationService = mocker.CreateInstance<ShiftValidationService>();
        }

        [Fact]
        public void Delete_ValidId_ShouldReturnOK()
        {
            // Arrange
            const int shiftId = 10;

            // Act
            var result = _shiftValidationService.IsvalidForDelete(shiftId);

            // Assert
            result.Success.ShouldBeTrue();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Delete_InvalidId_ShouldShowError_ShouldNotCallRepository(int shiftId)
        {
            // Arrange

            // Act
            var result = _shiftValidationService.IsvalidForDelete(shiftId);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors
                .Where(error => error.Property == nameof(shiftId))
                .Any(error => error.Value == Constants.ShiftValidationErrors.InvalidId)
                .ShouldBeTrue();
        }
    }

    public class SwapShiftTests
    {
        private readonly ShiftValidationService _shiftValidationService;
        private readonly Mock<IEmployeeRepository> _employeeRepositoryMock;
        private readonly Mock<IEmployeeValidationService> _employeeValidationService;

        public SwapShiftTests()
        {
            var mocker = new AutoMocker();
            _shiftValidationService = mocker.CreateSelfMock<ShiftValidationService>();
            mocker.GetMock<IShiftRepository>();
            _employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            _employeeValidationService = mocker.GetMock<IEmployeeValidationService>();
        }

        [Fact]
        public async Task Swap_IsValid_ShouldReturnOK()
        {
            // Arrange
            var firstShift = new Shift
            {
                Id = 1,
                EmployeeId = 11,
                From = new DateTime(2020, 10, 10),
                To = new DateTime(2020, 10, 10).AddHours(5)
            };
            var firstEmployee = new Employee();
            var secondShift = new Shift
            {
                Id = 2,
                EmployeeId = 22,
                From = new DateTime(2020, 11, 11),
                To = new DateTime(2020, 11, 11).AddHours(7)
            };
            var secondEmployee = new Employee();
            _employeeRepositoryMock.Setup(repository => repository.Get(firstShift.EmployeeId))
                .ReturnsAsync(firstEmployee);
            _employeeRepositoryMock.Setup(repository => repository.Get(secondShift.EmployeeId))
                .ReturnsAsync(secondEmployee);
            _employeeValidationService.Setup(service => service.IsEmployeeValid(firstEmployee))
                .Returns(new OperationResult(true));
            _employeeValidationService.Setup(service => service.IsEmployeeValid(secondEmployee))
                .Returns(new OperationResult(true));

            // Act
            var result = await _shiftValidationService.IsvalidForSwap(firstShift, secondShift);

            // Assert
            result.Success.ShouldBeTrue();
            // We need to validate the employee data
            _employeeValidationService.Verify(service => service.IsEmployeeValid(firstEmployee));
            _employeeValidationService.Verify(service => service.IsEmployeeValid(secondEmployee));
            // TODO: We need to validate the shifts
            // TODO: validate we called ValidateShift(), one per shift
        }
    }
}