﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using Moq.AutoMock;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services;
using Planday.Shifts.API.Services.Abstractions;
using Shouldly;
using Xunit;

namespace Planday.Shifts.API.Tests.Services
{
    public class EmployeeServiceTests
    {
        [Fact]
        public async Task Get_ShouldCallRepository()
        {
            // Arrange
            const int employeeId = 10;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EmployeeService>() as IEmployeeService;
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();

            // Act
            var employee = await service.Get(employeeId);
            employee.ShouldBeNull();
            employeeRepositoryMock.Verify(repository => repository.Get(employeeId), Times.Once);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task Get_InvalidId_ShouldReturnNull_ShouldNotCallRepository(int employeeId)
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EmployeeService>() as IEmployeeService;
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();

            // Act
            var employee = await service.Get(employeeId);
            employee.ShouldBeNull();
            employeeRepositoryMock.Verify(repository => repository.Get(employeeId), Times.Never);
        }

        [Fact]
        public async Task Add_ShouldCallRepository_ShouldCallValidationService()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EmployeeService>() as IEmployeeService;
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var employeeValidationServiceMock = mocker.GetMock<IEmployeeValidationService>();
            var mapperMock = mocker.GetMock<IMapper>();
            var employeeToAdd = new EmployeeAddRequest();
            var employee = new Employee();
            mapperMock.Setup(mapper => mapper.Map<Employee>(employeeToAdd)).Returns(employee);
            employeeValidationServiceMock
                .Setup(validationService =>
                    validationService.IsValidForAdd(employee))
                .ReturnsAsync(new OperationResult(true));

            // Act
            var result = await service.Add(employeeToAdd);

            // Assert
            result.Success.ShouldBeTrue();
            employeeValidationServiceMock.Verify(validationService => validationService.IsValidForAdd(employee));
            employeeRepositoryMock.Verify(repository => repository.Add(employee), Times.Once);
        }

        [Fact]
        public async Task Add_ValidationFails_ShouldNotCallRepository()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EmployeeService>() as IEmployeeService;
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var mapperMock = mocker.GetMock<IMapper>();
            var employeeToAdd = new EmployeeAddRequest();
            var employee = new Employee();
            mapperMock.Setup(mapper => mapper.Map<Employee>(employeeToAdd)).Returns(employee);
            var employeeValidationServiceMock = mocker.GetMock<IEmployeeValidationService>();
            employeeValidationServiceMock
                .Setup(validationService =>
                    validationService.IsValidForAdd(employee))
                .ReturnsAsync(new OperationResult(false, new List<ValidationError>()));

            // Act
            var result = await service.Add(employeeToAdd);

            // Assert
            result.Success.ShouldBeFalse();
            employeeRepositoryMock.Verify(repository => repository.Add(employee), Times.Never);
        }

        [Fact]
        public async Task Update_ShouldCallRepository_ShouldCallValidationService()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EmployeeService>() as IEmployeeService;
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var mapperMock = mocker.GetMock<IMapper>();
            var employeeToUpdate = new EmployeeUpdateRequest();
            var employee = new Employee
            {
                Id = 1,
                Email = "the@mail.com"
            };
            mapperMock.Setup(mapper => mapper.Map<Employee>(employeeToUpdate)).Returns(employee);
            var employeeValidationServiceMock = mocker.GetMock<IEmployeeValidationService>();
            employeeValidationServiceMock
                .Setup(validationService =>
                    validationService.IsValidForUpdate(employee))
                .ReturnsAsync(new OperationResult(true));

            // Act
            var result = await service.Update(employeeToUpdate);

            // Assert
            result.Success.ShouldBeTrue();
            employeeValidationServiceMock.Verify(validationService => validationService.IsValidForUpdate(employee));
            employeeRepositoryMock.Verify(repository => repository.Update(employee), Times.Once);
        }

        [Fact]
        public async Task Update_ValidationFails_ShouldNotCallRepository()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EmployeeService>() as IEmployeeService;
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var mapperMock = mocker.GetMock<IMapper>();
            var employeeToUpdate = new EmployeeUpdateRequest();
            var employee = new Employee
            {
                Id = 1,
                Email = "the@mail.com"
            };
            mapperMock.Setup(mapper => mapper.Map<Employee>(employeeToUpdate)).Returns(employee);
            var employeeValidationServiceMock = mocker.GetMock<IEmployeeValidationService>();
            employeeValidationServiceMock
                .Setup(validationService =>
                    validationService.IsValidForUpdate(employee))
                .ReturnsAsync(new OperationResult(false, new List<ValidationError>()));

            // Act
            var result = await service.Update(employeeToUpdate);

            // Assert
            result.Success.ShouldBeFalse();
            employeeRepositoryMock.Verify(repository => repository.Update(employee), Times.Never);
        }

        [Fact]
        public async Task Delete_ShouldCallRepository_ShouldCallValidationService()
        {
            // Arrange
            const int employeeId = 1;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EmployeeService>() as IEmployeeService;
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var employeeValidationServiceMock = mocker.GetMock<IEmployeeValidationService>();
            employeeValidationServiceMock
                .Setup(validationService =>
                    validationService.IsValidForDelete(employeeId))
                .Returns(new OperationResult(true));

            // Act
            var result = await service.Delete(employeeId);
            result.Success.ShouldBeTrue();
            employeeValidationServiceMock.Verify(validationService => validationService.IsValidForDelete(employeeId),
                Times.Once);
            employeeRepositoryMock.Verify(repository => repository.Delete(employeeId), Times.Once);
        }

        [Fact]
        public async Task Delete_ValidationFails_ShouldNotCallRepository()
        {
            // Arrange
            const int employeeId = 1;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<EmployeeService>() as IEmployeeService;
            var employeeRepositoryMock = mocker.GetMock<IEmployeeRepository>();
            var employeeValidationServiceMock = mocker.GetMock<IEmployeeValidationService>();
            employeeValidationServiceMock
                .Setup(validationService =>
                    validationService.IsValidForDelete(employeeId))
                .Returns(new OperationResult(false));

            // Act
            var result = await service.Delete(employeeId);

            // Assert
            result.Success.ShouldBeFalse();
            employeeValidationServiceMock.Verify(validationService => validationService.IsValidForDelete(employeeId));
            employeeRepositoryMock.Verify(repository => repository.Delete(employeeId), Times.Never);
        }
    }
}