﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using Moq.AutoMock;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services;
using Planday.Shifts.API.Services.Abstractions;
using Shouldly;
using Xunit;

namespace Planday.Shifts.API.Tests.Services
{
    public class ShiftServiceTests
    {
        [Fact]
        public async Task Get_SpecificEmployee_ShouldReturnShifts()
        {
            // Arrange
            const int employeeId = 100;
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<ShiftService>() as IShiftService;
            var shifts = new List<Shift>
            {
                new Shift(),
                new Shift()
            };
            var returnedShifts = new List<ShiftResult>
            {
                new ShiftResult(),
                new ShiftResult()
            };
            var shiftRepositoryMock = mocker.GetMock<IShiftRepository>();
            shiftRepositoryMock
                .Setup(repository => repository.GetByEmployeeId(employeeId))
                .ReturnsAsync(shifts);
            var mapperMock = mocker.GetMock<IMapper>();
            mapperMock.Setup(mapper => mapper.Map<IEnumerable<ShiftResult>>(shifts)).Returns(returnedShifts);

            // Act
            var result = await service.Get(employeeId);

            // Assert
            shiftRepositoryMock.Verify(repository => repository.GetByEmployeeId(employeeId), Times.Once);
            result.ShouldBe(returnedShifts);
        }

        [Fact]
        public async Task GetAll_ShouldReturnShifts()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<ShiftService>() as IShiftService;
            var shifts = new List<Shift>
            {
                new Shift(),
                new Shift()
            };
            var shiftRepositoryMock = mocker.GetMock<IShiftRepository>();
            shiftRepositoryMock
                .Setup(repository => repository.Get())
                .ReturnsAsync(shifts);
            var returnedShifts = new List<ShiftResult>
            {
                new ShiftResult(),
                new ShiftResult()
            };
            var mapperMock = mocker.GetMock<IMapper>();
            mapperMock.Setup(mapper => mapper.Map<IEnumerable<ShiftResult>>(shifts)).Returns(returnedShifts);

            // Act
            var result = await service.Get();

            // Assert
            shiftRepositoryMock.Verify(repository => repository.Get(), Times.Once);
            result.ShouldBe(returnedShifts);
        }

        [Fact]
        public async Task Add_ShouldCallRepository()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<ShiftService>() as IShiftService;
            var shiftRepositoryMock = mocker.GetMock<IShiftRepository>();
            var shiftValidationServiceMock = mocker.GetMock<IShiftValidationService>();
            var shift = new ShiftAddRequest
            {
                EmployeeId = 1,
                From = new DateTime(2020, 10, 10).AddHours(12).AddMinutes(15),
                To = new DateTime(2020, 10, 10).AddHours(15)
            };
            shiftValidationServiceMock
                .Setup(validationService => validationService.IsvalidForAdd(It.IsAny<Shift>()))
                .ReturnsAsync(new OperationResult(true));

            // Act
            var result = await service.Add(shift);

            // Assert
            result.Success.ShouldBeTrue();
            shiftRepositoryMock.Verify(repository => repository.Add(It.IsAny<Shift>()), Times.Once);
        }

        [Fact]
        public async Task Add_ShiftNotValid_ShouldShowError()
        {
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<ShiftService>() as IShiftService;
            var shiftRepositoryMock = mocker.GetMock<IShiftRepository>();
            var shift = new ShiftAddRequest
            {
                EmployeeId = 1,
                From = DateTime.Now,
                To = DateTime.Now
            };
            var shiftValidationErrors = new List<ValidationError> {new ValidationError("", "")};
            var shiftValidationServiceMock = mocker.GetMock<IShiftValidationService>();
            shiftValidationServiceMock
                .Setup(validationService => validationService.IsvalidForAdd(It.IsAny<Shift>()))
                .ReturnsAsync(new OperationResult(false, shiftValidationErrors));

            // Act
            var result = await service.Add(shift);

            // Assert
            result.Success.ShouldBeFalse();
            result.ValidationErrors.Count().ShouldBe(shiftValidationErrors.Count);
            shiftValidationServiceMock.Verify(validationService => validationService.IsvalidForAdd(It.IsAny<Shift>()));
            shiftRepositoryMock.Verify(repository => repository.Add(It.IsAny<Shift>()), Times.Never);
        }

        [Fact]
        public async Task Update_ShouldCallRepository_ShouldCallValidationService()
        {
            // Arrange
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<ShiftService>() as IShiftService;
            var shiftRepositoryMock = mocker.GetMock<IShiftRepository>();
            var shiftValidationServiceMock = mocker.GetMock<IShiftValidationService>();
            var shiftUpdate = new ShiftUpdateRequest
            {
                Id = 10,
                From = new DateTime(2020, 10, 10).AddHours(12).AddMinutes(15),
                To = new DateTime(2020, 10, 10).AddHours(15)
            };
            var shift = new Shift();
            shiftRepositoryMock.Setup(repository => repository.Get(shiftUpdate.Id))
                .ReturnsAsync(shift);
            shiftValidationServiceMock
                .Setup(validationService => validationService.IsvalidForUpdate(shift))
                .ReturnsAsync(new OperationResult(true));

            // Act
            var result = await service.Update(shiftUpdate);

            // Assert
            result.Success.ShouldBeTrue();
            shiftRepositoryMock.Verify(repository => repository.Update(shift), Times.Once);
        }

        [Fact]
        public async Task Update_ShiftNotValid_ShouldShowError()
        {
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<ShiftService>() as IShiftService;
            var shiftRepositoryMock = mocker.GetMock<IShiftRepository>();
            var shiftUpdate = new ShiftUpdateRequest
            {
                Id = 10,
                From = DateTime.Now,
                To = DateTime.Now
            };
            var shift = new Shift();
            shiftRepositoryMock.Setup(repository => repository.Get(shiftUpdate.Id))
                .ReturnsAsync(shift);
            var operationResult = new OperationResult(false, new List<ValidationError>());
            var shiftValidationServiceMock = mocker.GetMock<IShiftValidationService>();
            shiftValidationServiceMock
                .Setup(validationService => validationService.IsvalidForUpdate(shift))
                .ReturnsAsync(operationResult);

            // Act
            var result = await service.Update(shiftUpdate);

            // Assert
            result.Success.ShouldBeFalse();
            result.ShouldBe(operationResult);
            shiftValidationServiceMock.Verify(validationService => validationService.IsvalidForUpdate(shift));
            shiftRepositoryMock.Verify(repository => repository.Update(shift), Times.Never);
        }

        [Fact]
        public async Task Delete_ShouldCallRepository_ShouldCallValidationService()
        {
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<ShiftService>() as IShiftService;
            var shiftRepositoryMock = mocker.GetMock<IShiftRepository>();
            var operationResult = new OperationResult(true);
            var shiftValidationServiceMock = mocker.GetMock<IShiftValidationService>();
            const int shiftId = 1;
            shiftValidationServiceMock
                .Setup(validationService => validationService.IsvalidForDelete(shiftId))
                .Returns(operationResult);

            // Act
            var result = await service.Delete(shiftId);

            // Assert
            result.Success.ShouldBeTrue();
            shiftValidationServiceMock.Verify(validationService => validationService.IsvalidForDelete(shiftId));
            shiftRepositoryMock.Verify(repository => repository.Delete(shiftId), Times.Once);
        }

        [Fact]
        public async Task Switch_ShouldCallRepository_ShouldCallValidationService()
        {
            var mocker = new AutoMocker();
            var service = mocker.CreateInstance<ShiftService>() as IShiftService;
            var shiftRepositoryMock = mocker.GetMock<IShiftRepository>();
            var operationResult = new OperationResult(true);
            var shiftValidationServiceMock = mocker.GetMock<IShiftValidationService>();
            const int employeeIdInFirstShift = 12;
            const int employeeIdInSecondShift = 13;
            var firstShift = new Shift
            {
                Id = 1,
                EmployeeId = employeeIdInFirstShift
            };
            var secondShift = new Shift
            {
                Id = 2,
                EmployeeId = employeeIdInSecondShift
            };
            shiftRepositoryMock.Setup(repository => repository.Get(firstShift.Id))
                .ReturnsAsync(firstShift);
            shiftRepositoryMock.Setup(repository => repository.Get(secondShift.Id))
                .ReturnsAsync(secondShift);
            shiftValidationServiceMock
                .Setup(validationService => validationService.IsvalidForSwap(firstShift, secondShift))
                .ReturnsAsync(operationResult);

            // Act
            var result = await service.Swap(firstShift.Id, secondShift.Id);

            // Assert
            result.Success.ShouldBeTrue();
            shiftValidationServiceMock.Verify(validationService =>
                validationService.IsvalidForSwap(firstShift, secondShift));
            shiftRepositoryMock.Verify(repository => repository.Update(firstShift), Times.Once);
            shiftRepositoryMock.Verify(repository => repository.Update(secondShift), Times.Once);
            firstShift.EmployeeId.ShouldBe(employeeIdInSecondShift);
            secondShift.EmployeeId.ShouldBe(employeeIdInFirstShift);
        }

        //TODO: Create, Edit, Delete shifts
    }
}