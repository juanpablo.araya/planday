﻿using System;
using System.Configuration;
using System.Reflection;
using DbUp;

namespace Planday.Shifts.DbUpdater
{
    internal static class Program
    {
        private static void Main()
        {
            var connectionString =
                ConfigurationManager.ConnectionStrings["Planday"].ConnectionString;

            var result = DeployChanges.To
                .SqlDatabase(connectionString)
                .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                .LogToConsole()
                .Build()
                .PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
                return;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();
        }
    }
}