﻿CREATE TABLE Employees(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
    [Email] VARCHAR(255) NOT NULL,
    [FirstName] VARCHAR(255) NULL,
    [LastName] VARCHAR(255) NULL,
    [Deleted] BIT NOT NULL DEFAULT 0
)
--CREATE UNIQUE INDEX Unique_Employees_Email ON Employees(Email);
--We will not use unique constraint, as we have soft delete
CREATE NONCLUSTERED INDEX IX_Employees_Email ON Employees (Email);
CREATE NONCLUSTERED INDEX IX_Employees_Deleted ON Employees (Deleted);

CREATE TABLE Shifts(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
    [EmployeeId] INT NOT NULL,
    CONSTRAINT FK_Shifts_Employee FOREIGN KEY (EmployeeId)
        REFERENCES Employees (Id),
    [From] DATETIME NOT NULL,
    [To] DATETIME NOT NULL,
    [Deleted] BIT NOT NULL DEFAULT 0
)
CREATE NONCLUSTERED INDEX IX_Shifts_EmployeeId ON Shifts (Id);
CREATE NONCLUSTERED INDEX IX_Shifts_Deleted ON Shifts (Deleted);
