﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services.Abstractions;

namespace Planday.Shifts.API.Services
{
    public class EmployeeValidationService : IEmployeeValidationService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeValidationService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository ?? throw new ArgumentNullException(nameof(employeeRepository));
        }

        public async Task<OperationResult> IsValidForAdd(Employee employee)
        {
            var validationErrors = new List<ValidationError>();

            ValidateEmployee(employee, validationErrors);

            if (validationErrors.Any()) 
                return new OperationResult(validationErrors);

            var employeeByMail = await _employeeRepository.FindByEmail(employee.Email);
            // We need to check if there is another employee with that email
            if (employeeByMail != null)
                validationErrors.Add(new ValidationError(nameof(Employee.Email),
                    Constants.EmployeeValidationErrors.EmailAlreadyExists));

            return new OperationResult(validationErrors);
        }

        public async Task<OperationResult> IsValidForUpdate(Employee employee)
        {
            var validationErrors = new List<ValidationError>();

            ValidateId(employee, validationErrors);
            ValidateEmployee(employee, validationErrors);

            if (validationErrors.Any())
                return new OperationResult(validationErrors);

            var employeeByMail = await _employeeRepository.FindByEmail(employee.Email);

            // Checking if there is another employee with that email
            if (employeeByMail != null && !Equals(employee, employeeByMail))
                validationErrors.Add(new ValidationError(nameof(Employee.Email),
                    Constants.EmployeeValidationErrors.EmailAlreadyExists));

            return new OperationResult(validationErrors);
        }

        public OperationResult IsValidForDelete(int employeeId)
        {
            if (employeeId <= 0)
                return new OperationResult(false, new List<ValidationError>
                {
                    new ValidationError(nameof(Employee.Id), Constants.EmployeeValidationErrors.InvalidId)
                });

            return new OperationResult(true);
        }

        public OperationResult IsEmployeeValid(Employee employee)
        {
            var validationErrors = new List<ValidationError>();

            ValidateEmployee(employee, validationErrors);
            return new OperationResult(validationErrors);
        }

        private void ValidateEmployee(Employee employee, List<ValidationError> validationErrors)
        {
            ValidateEmail(employee, validationErrors);
            ValidateFirsName(employee, validationErrors);
            ValidateLastName(employee, validationErrors);
        }

        private void ValidateEmail(Employee employee, ICollection<ValidationError> validationErrors)
        {
            if (!IsValidEmail(employee))
                validationErrors.Add(new ValidationError(nameof(Employee.Email),
                    Constants.EmployeeValidationErrors.InvalidEmail));
        }

        private static void ValidateId(Employee employee, ICollection<ValidationError> validationErrors)
        {
            if (employee.Id <= 0)
                validationErrors.Add(new ValidationError(nameof(Employee.Id),
                    Constants.EmployeeValidationErrors.InvalidId));
        }

        private static bool IsValidEmail(Employee employee)
        {
            if (string.IsNullOrWhiteSpace(employee.Email))
                return false;

            try
            {
                var mailAddress = new System.Net.Mail.MailAddress(employee.Email);
                return mailAddress.Address == employee.Email;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static void ValidateLastName(Employee employee, ICollection<ValidationError> validationErrors)
        {
            if (string.IsNullOrWhiteSpace(employee.LastName))
                validationErrors.Add(new ValidationError(nameof(Employee.LastName),
                    Constants.EmployeeValidationErrors.InvalidLastName));
        }

        private static void ValidateFirsName(Employee employee, ICollection<ValidationError> validationErrors)
        {
            if (string.IsNullOrWhiteSpace(employee.FirstName))
                validationErrors.Add(new ValidationError(nameof(Employee.FirstName),
                    Constants.EmployeeValidationErrors.InvalidFirstName));
        }
    }
}