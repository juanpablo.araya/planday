﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services.Abstractions;

namespace Planday.Shifts.API.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeValidationService _employeeValidationService;
        private readonly IMapper _mapper;

        public EmployeeService(
            IEmployeeRepository employeeRepository,
            IEmployeeValidationService employeeValidationService,
            IMapper mapper)
        {
            _employeeRepository = employeeRepository ?? throw new ArgumentNullException(nameof(employeeRepository));
            _employeeValidationService = employeeValidationService ??
                                         throw new ArgumentNullException(nameof(employeeValidationService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<EmployeeResponse> Get(int employeeId)
        {
            if (employeeId <= 0) 
                return null;

            var employee = await _employeeRepository.Get(employeeId);

            return _mapper.Map<EmployeeResponse>(employee);
        }

        public async Task<OperationResult> Add(EmployeeAddRequest employeeToAdd)
        {
            var employee = _mapper.Map<Employee>(employeeToAdd);
            var validationResult = await _employeeValidationService.IsValidForAdd(employee);
            if (!validationResult.Success) 
                return validationResult;

            await _employeeRepository.Add(employee);
            return new OperationResult(true);
        }

        public async Task<OperationResult> Update(EmployeeUpdateRequest employeeToUpdate)
        {
            var employee = _mapper.Map<Employee>(employeeToUpdate);
            var validationResult = await _employeeValidationService.IsValidForUpdate(employee);
            if (!validationResult.Success)
                return validationResult;

            await _employeeRepository.Update(employee);
            return new OperationResult(true);
        }

        public async Task<OperationResult> Delete(int employeeId)
        {
            var validationResult = _employeeValidationService.IsValidForDelete(employeeId);
            if (!validationResult.Success)
                return validationResult;

            await _employeeRepository.Delete(employeeId);
            return new OperationResult(true);
        }
    }
}