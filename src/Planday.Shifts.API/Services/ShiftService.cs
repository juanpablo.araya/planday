﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services.Abstractions;

namespace Planday.Shifts.API.Services
{
    public class ShiftService : IShiftService
    {
        private readonly IShiftRepository _shiftRepository;
        private readonly IShiftValidationService _shiftValidationService;
        private readonly IMapper _mapper;

        public ShiftService(
            IShiftRepository shiftRepository,
            IShiftValidationService validationService,
            IMapper mapper)
        {
            _shiftRepository = shiftRepository ?? throw new ArgumentNullException(nameof(shiftRepository));
            _shiftValidationService = validationService ?? throw new ArgumentNullException(nameof(validationService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<OperationResult> Add(ShiftAddRequest shiftToAdd)
        {
            var shift = new Shift
            {
                EmployeeId = shiftToAdd.EmployeeId,
                From = shiftToAdd.From,
                To = shiftToAdd.To
            };

            var result = await _shiftValidationService.IsvalidForAdd(shift);
            if (!result.Success)
                return result;

            await _shiftRepository.Add(shift);
            return new OperationResult(true);
        }

        public async Task<OperationResult> Update(ShiftUpdateRequest shiftToUpdate)
        {
            var shift = await _shiftRepository.Get(shiftToUpdate.Id);
            if (shift == null)
                return new OperationResult(false, new List<ValidationError>
                {
                    new ValidationError(nameof(shiftToUpdate.Id), Constants.ShiftValidationErrors.ShiftDoesNotExist)
                });
            shift.From = shiftToUpdate.From;
            shift.To = shiftToUpdate.To;
            var result = await _shiftValidationService.IsvalidForUpdate(shift);
            if (!result.Success)
                return result;

            await _shiftRepository.Update(shift);
            return new OperationResult(true);
        }

        public async Task<IEnumerable<ShiftResult>> Get(int employeeId)
        {
            var shifts = await _shiftRepository.GetByEmployeeId(employeeId);
            return _mapper.Map<IEnumerable<ShiftResult>>(shifts);
        }

        public async Task<IEnumerable<ShiftResult>> Get()
        {
            var shifts = await _shiftRepository.Get();
            return _mapper.Map<IEnumerable<ShiftResult>>(shifts);
        }

        public async Task<OperationResult> Delete(int shiftId)
        {
            var result = _shiftValidationService.IsvalidForDelete(shiftId);
            if (!result.Success)
                return result;

            await _shiftRepository.Delete(shiftId);
            return new OperationResult(true);
        }

        public async Task<OperationResult> Swap(int shiftId1, int shiftId2)
        {
            var firstShift = await _shiftRepository.Get(shiftId1);
            var secondShift = await _shiftRepository.Get(shiftId2);

            // We only allow to swap if the shifts exist
            if (firstShift == null)
                return new OperationResult(false, new List<ValidationError>
                {
                    new ValidationError(nameof(shiftId1),
                        $"{nameof(shiftId1)} {Constants.ShiftValidationErrors.ShiftDoesNotExist}")
                });

            if (secondShift == null)
                return new OperationResult(false, new List<ValidationError>
                {
                    new ValidationError(nameof(shiftId1),
                        $"{nameof(shiftId2)} {Constants.ShiftValidationErrors.ShiftDoesNotExist}")
                });

            var result = await _shiftValidationService.IsvalidForSwap(firstShift, secondShift);

            if (!result.Success)
                return result;

            var firstShiftEmployeeId = firstShift.EmployeeId;
            firstShift.EmployeeId = secondShift.EmployeeId;
            secondShift.EmployeeId = firstShiftEmployeeId;

            await _shiftRepository.Update(firstShift);
            await _shiftRepository.Update(secondShift);
            return new OperationResult(true);
        }
    }
}