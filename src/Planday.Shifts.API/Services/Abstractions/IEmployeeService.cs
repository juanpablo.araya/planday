﻿using System.Threading.Tasks;
using Planday.Shifts.API.Models;

namespace Planday.Shifts.API.Services.Abstractions
{
    public interface IEmployeeService
    {
        Task<EmployeeResponse> Get(int employeeId);
        Task<OperationResult> Add(EmployeeAddRequest employeeToAdd);
        Task<OperationResult> Update(EmployeeUpdateRequest employeeToUpdate);
        Task<OperationResult> Delete(int employeeId);
    }
}