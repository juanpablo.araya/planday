﻿using System.Threading.Tasks;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;

namespace Planday.Shifts.API.Services.Abstractions
{
    public interface IShiftValidationService
    {
        Task<OperationResult> IsvalidForAdd(Shift shift);
        Task<OperationResult> IsvalidForUpdate(Shift shift);
        OperationResult IsvalidForDelete(int shiftId);
        Task<OperationResult> IsvalidForSwap(Shift firstShift, Shift secondShift);
    }
}