﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Planday.Shifts.API.Models;

namespace Planday.Shifts.API.Services.Abstractions
{
    public interface IShiftService
    {
        Task<OperationResult> Add(ShiftAddRequest shiftToAdd);
        Task<OperationResult> Update(ShiftUpdateRequest shiftToUpdate);
        Task<IEnumerable<ShiftResult>> Get(int employeeId);
        Task<IEnumerable<ShiftResult>> Get();
        Task<OperationResult> Delete(int shiftId);
        Task<OperationResult> Swap(int shiftId1, int shiftId2);
    }
}