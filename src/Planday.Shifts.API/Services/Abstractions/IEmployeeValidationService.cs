﻿using System.Threading.Tasks;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;

namespace Planday.Shifts.API.Services.Abstractions
{
    public interface IEmployeeValidationService
    {
        Task<OperationResult> IsValidForAdd(Employee employee);
        Task<OperationResult> IsValidForUpdate(Employee employee);
        OperationResult IsValidForDelete(int employeeId);
        OperationResult IsEmployeeValid(Employee employee);
    }
}