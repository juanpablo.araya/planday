﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services.Abstractions;

namespace Planday.Shifts.API.Services
{
    public class ShiftValidationService : IShiftValidationService
    {
        private readonly IShiftRepository _shiftRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeValidationService _employeeValidationService;

        public ShiftValidationService(
            IShiftRepository shiftRepository,
            IEmployeeRepository employeeRepository,
            IEmployeeValidationService employeeValidationService)
        {
            _shiftRepository = shiftRepository ?? throw new ArgumentNullException(nameof(shiftRepository));
            _employeeRepository = employeeRepository ?? throw new ArgumentNullException(nameof(employeeRepository));
            _employeeValidationService = employeeValidationService ??
                                         throw new ArgumentNullException(nameof(employeeValidationService));
        }

        public async Task<OperationResult> IsvalidForAdd(Shift shift)
        {
            var validationErrors = new List<ValidationError>();
            await ValidateShift(shift, validationErrors);

            return new OperationResult(validationErrors);
        }

        public async Task<OperationResult> IsvalidForUpdate(Shift shift)
        {
            var validationErrors = new List<ValidationError>();
            var shiftToModify = await _shiftRepository.Get(shift.Id);
            if (shiftToModify == null)
                validationErrors.Add(new ValidationError(nameof(Shift.Id),
                    Constants.ShiftValidationErrors.ShiftDoesNotExist));
            await ValidateShift(shift, validationErrors);

            return new OperationResult(validationErrors);
        }

        public OperationResult IsvalidForDelete(int shiftId)
        {
            var validationErrors = new List<ValidationError>();

            if (shiftId > 0)
                return new OperationResult(true);
            
            validationErrors.Add(new ValidationError(nameof(shiftId), Constants.ShiftValidationErrors.InvalidId));
            return new OperationResult(validationErrors);
        }

        public async Task<OperationResult> IsvalidForSwap(Shift firstShift, Shift secondShift)
        {
            var validationErrors = new List<ValidationError>();

            var firstEmployee = await _employeeRepository.Get(firstShift.EmployeeId);
            var secondEmployee = await _employeeRepository.Get(secondShift.EmployeeId);

            var result = _employeeValidationService.IsEmployeeValid(firstEmployee);
            validationErrors.AddRange(result.ValidationErrors);

            result = _employeeValidationService.IsEmployeeValid(secondEmployee);
            validationErrors.AddRange(result.ValidationErrors);

            await ValidateShift(secondEmployee, firstShift, validationErrors, secondShift);
            await ValidateShift(firstEmployee, secondShift, validationErrors, firstShift);

            return new OperationResult(validationErrors);
        }

        private async Task ValidateShift(Shift shiftToInclude, List<ValidationError> validationErrors)
        {
            var employee = await _employeeRepository.Get(shiftToInclude.EmployeeId);
            await ValidateShift(employee, shiftToInclude, validationErrors);
        }

        private async Task ValidateShift(Employee employee, Shift shiftToInclude,
            List<ValidationError> validationErrors, Shift shiftToRemoveFromCalculation = null)
        {
            shiftToRemoveFromCalculation ??= shiftToInclude;

            if (employee == null)
            {
                validationErrors.Add(new ValidationError(nameof(Shift.EmployeeId),
                    Constants.ShiftValidationErrors.EmployeeDoesNotExist));
                return;
            }

            if (shiftToInclude.To <= shiftToInclude.From)
                validationErrors.Add(new ValidationError(nameof(Shift.To),
                    Constants.ShiftValidationErrors.ToDateStartsBeforeFromDate));

            var employeeShifts = await _shiftRepository.GetByEmployeeId(employee.Id);
            // If the shift has Id, we will exclude it from the list of shifts to validate, as we are
            // "moving it" (update)
            if (shiftToRemoveFromCalculation.Id > 0)
                employeeShifts = employeeShifts.Where(shift1 => shift1.Id != shiftToRemoveFromCalculation.Id);

            // Rules to detect a shift cannot be assigned:
            // - Start or EndDate are contained in an existing shift
            // ... or the reverse:
            // - The shift to add contains any start or end date of existing shifts

            if (employeeShifts.Any(existingShift => existingShift.Overlaps(shiftToInclude)) ||
                employeeShifts.Any(existingShift => shiftToInclude.Overlaps(existingShift)))
                validationErrors.Add(new ValidationError(nameof(Shift),
                    Constants.ShiftValidationErrors.EmployeeAlreadyHasShiftsAtThatRange));
        }
    }
}