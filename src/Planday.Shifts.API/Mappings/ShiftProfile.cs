﻿using AutoMapper;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;

namespace Planday.Shifts.API.Mappings
{
    public class ShiftProfile : Profile
    {
        public ShiftProfile()
        {
            CreateMap<Shift, ShiftResult>();
        }
    }
}