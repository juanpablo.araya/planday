﻿using AutoMapper;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Models;

namespace Planday.Shifts.API.Mappings
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<EmployeeAddRequest, Employee>();
            CreateMap<EmployeeUpdateRequest, Employee>();
            CreateMap<Employee, EmployeeResponse>();
        }
    }
}