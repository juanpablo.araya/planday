﻿namespace Planday.Shifts.API.Models
{
    public class EmployeeAddRequest
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}