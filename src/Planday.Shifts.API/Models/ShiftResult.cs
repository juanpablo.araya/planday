﻿using System;

namespace Planday.Shifts.API.Models
{
    public class ShiftResult
    {
        public int Id { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int EmployeeId { get; set; }
    }
}