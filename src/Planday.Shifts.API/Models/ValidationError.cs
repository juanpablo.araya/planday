﻿namespace Planday.Shifts.API.Models
{
    public class ValidationError
    {
        public string Property { get; }
        public string Value { get; }

        public ValidationError(string property, string value)
        {
            Property = property;
            Value = value;
        }
    }
}