﻿using System;

namespace Planday.Shifts.API.Models
{
    public class ShiftAddRequest
    {
        public int EmployeeId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}