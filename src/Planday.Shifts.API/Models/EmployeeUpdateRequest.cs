﻿namespace Planday.Shifts.API.Models
{
    public class EmployeeUpdateRequest : EmployeeAddRequest
    {
        public int Id { get; set; }
    }
}