﻿namespace Planday.Shifts.API.Models
{
    public static class Constants
    {
        public static class EmployeeValidationErrors
        {
            public static string EmailAlreadyExists => "Email already exists";
            public static string InvalidEmail => "Invalid Email";
            public static string InvalidFirstName => "Invalid First Name";
            public static string InvalidLastName => "Invalid Last Name";
            public static string InvalidId => "Invalid Id";
        }

        public static class ShiftValidationErrors
        {
            public static string EmployeeDoesNotExist => "Employee does not exist";

            public static string EmployeeAlreadyHasShiftsAtThatRange =>
                "Employee already has shifts at that time range";

            public static string ToDateStartsBeforeFromDate => "To date starts before From date";
            public static string ShiftDoesNotExist => "Shift does not exist";
            public static string InvalidId => "Invalid Id";
        }
    }
}