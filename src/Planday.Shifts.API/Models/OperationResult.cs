﻿using System.Collections.Generic;
using System.Linq;

namespace Planday.Shifts.API.Models
{
    public class OperationResult
    {
        public bool Success { get; }
        public IEnumerable<ValidationError> ValidationErrors { get; }

        public OperationResult(bool success, IEnumerable<ValidationError> validationErrors = null)
        {
            Success = success;
            ValidationErrors = validationErrors ?? Enumerable.Empty<ValidationError>();
        }

        public OperationResult(IEnumerable<ValidationError> validationErrors)
        {
            ValidationErrors = validationErrors ?? Enumerable.Empty<ValidationError>();
            Success = !validationErrors.Any();
        }
    }
}