﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Planday.Shifts.API.Entities;
using Planday.Shifts.API.Repositories.Abstractions;

namespace Planday.Shifts.API.Repositories
{
    public class ShiftRepository : IShiftRepository
    {
        private readonly ShiftContext _dbContext;

        public ShiftRepository(ShiftContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task Add(Shift shift)
        {
            await _dbContext.Shifts.AddAsync(shift);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(Shift shift)
        {
            _dbContext.Update(shift);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Shift>> Get()
        {
            return await _dbContext.Shifts.ToListAsync();
        }

        public async Task Delete(int shiftId)
        {
            var shift = await _dbContext.Shifts.FindAsync(shiftId);
            if (shift == null)
                return;
            
            _dbContext.Shifts.Remove(shift);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Shift> Get(int shiftId)
        {
            return await _dbContext.Shifts.FindAsync(shiftId);
        }

        public async Task<IEnumerable<Shift>> GetByEmployeeId(int employeeId)
        {
            return await _dbContext.Shifts
                .Where(shift => shift.EmployeeId == employeeId)
                .ToListAsync();
        }
    }
}