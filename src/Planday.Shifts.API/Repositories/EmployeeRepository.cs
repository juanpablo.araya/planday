﻿using System;
using System.Threading.Tasks;
using Planday.Shifts.API.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using Planday.Shifts.API.Entities;

namespace Planday.Shifts.API.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly ShiftContext _dbContext;

        public EmployeeRepository(ShiftContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<Employee> Get(int employeeId)
        {
            return await _dbContext.Employees.FindAsync(employeeId);
        }

        public async Task Add(Employee employee)
        {
            await _dbContext.Employees.AddAsync(employee);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Employee> FindByEmail(string email)
        {
            return await _dbContext.Employees.FirstOrDefaultAsync(employee => employee.Email == email);
        }

        public async Task Update(Employee employee)
        {
            _dbContext.Update(employee);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(int employeeId)
        {
            var employee = await _dbContext.Employees.FindAsync(employeeId);
            if (employee == null)
                return;
            
            _dbContext.Employees.Remove(employee);
            await _dbContext.SaveChangesAsync();
        }
    }
}