﻿using System.Threading.Tasks;
using Planday.Shifts.API.Entities;

namespace Planday.Shifts.API.Repositories.Abstractions
{
    public interface IEmployeeRepository
    {
        Task<Employee> Get(int employeeId);
        Task Add(Employee employee);
        Task<Employee> FindByEmail(string email);
        Task Update(Employee employee);
        Task Delete(int employeeId);
    }
}