﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Planday.Shifts.API.Entities;

namespace Planday.Shifts.API.Repositories.Abstractions
{
    public interface IShiftRepository
    {
        Task Add(Shift shift);
        Task Update(Shift shift);
        Task<IEnumerable<Shift>> GetByEmployeeId(int employeeId);
        Task<IEnumerable<Shift>> Get();
        Task Delete(int shiftId);
        Task<Shift> Get(int shiftId);
    }
}