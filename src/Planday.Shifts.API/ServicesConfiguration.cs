﻿using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Planday.Shifts.API.Repositories;
using Planday.Shifts.API.Repositories.Abstractions;
using Planday.Shifts.API.Services;
using Planday.Shifts.API.Services.Abstractions;

namespace Planday.Shifts.API
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            // TODO: check scopes
            return services
                .AddScoped<IEmployeeService, EmployeeService>()
                .AddScoped<IEmployeeValidationService, EmployeeValidationService>()
                .AddScoped<IShiftService, ShiftService>()
                .AddScoped<IShiftValidationService, ShiftValidationService>();
        }

        public static IServiceCollection ConfigureRepositories(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<ShiftContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("Planday")));

            return services
                .AddScoped<IEmployeeRepository, EmployeeRepository>()
                .AddScoped<IShiftRepository, ShiftRepository>();
        }

        public static IServiceCollection ConfigureMappings(this IServiceCollection services)
        {
            return services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        }
    }
}