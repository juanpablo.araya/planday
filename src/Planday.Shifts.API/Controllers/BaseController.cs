﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Planday.Shifts.API.Models;

namespace Planday.Shifts.API.Controllers
{
    public class BaseController : Controller
    {
        protected IActionResult OkOrBadRequestResult(OperationResult operationResult)
        {
            return operationResult.Success
                ? (IActionResult) Ok()
                : BadRequest(operationResult.ValidationErrors.Select(error => error.Value));
        }
    }
}