﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Services.Abstractions;

namespace Planday.Shifts.API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class EmployeesController : BaseController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService ?? throw new ArgumentNullException(nameof(employeeService));
        }

        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _employeeService.Get(id));
        }

        [HttpPost]
        public async Task<IActionResult> Post(EmployeeAddRequest employee)
        {
            var result = await _employeeService.Add(employee);
            return OkOrBadRequestResult(result);
        }

        [HttpPatch]
        public async Task<IActionResult> Patch(EmployeeUpdateRequest employee)
        {
            var result = await _employeeService.Update(employee);
            return OkOrBadRequestResult(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int employeeId)
        {
            var result = await _employeeService.Delete(employeeId);
            return OkOrBadRequestResult(result);
        }
    }
}