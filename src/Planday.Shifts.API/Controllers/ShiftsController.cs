﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Planday.Shifts.API.Models;
using Planday.Shifts.API.Services.Abstractions;

namespace Planday.Shifts.API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class ShiftsController : BaseController
    {
        private readonly IShiftService _shiftService;

        public ShiftsController(IShiftService shiftService)
        {
            _shiftService = shiftService ?? throw new ArgumentNullException(nameof(shiftService));
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _shiftService.Get());
        }

        [HttpGet]
        [Route("{employeeId}")]
        public async Task<IActionResult> GetByEmployeeId(int employeeId)
        {
            return Ok(await _shiftService.Get(employeeId));
        }

        [HttpPost]
        public async Task<IActionResult> Post(ShiftAddRequest shift)
        {
            var result = await _shiftService.Add(shift);
            return OkOrBadRequestResult(result);
        }

        [HttpPatch]
        public async Task<IActionResult> Patch(ShiftUpdateRequest shift)
        {
            var result = await _shiftService.Update(shift);
            return OkOrBadRequestResult(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int shiftId)
        {
            var result = await _shiftService.Delete(shiftId);
            return OkOrBadRequestResult(result);
        }

        [HttpPatch]
        [Route("swap/{shiftId1}/{shiftId2}")]
        public async Task<IActionResult> Swap(int shiftId1, int shiftId2)
        {
            var result = await _shiftService.Swap(shiftId1, shiftId2);
            return OkOrBadRequestResult(result);
        }
    }
}