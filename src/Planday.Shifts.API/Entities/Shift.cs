﻿using System;

namespace Planday.Shifts.API.Entities
{
    public class Shift
    {
        public int Id { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int EmployeeId { get; set; }
        public bool Deleted { get; set; }

        public bool Overlaps(Shift shift)
        {
            // Overlaps if shift.From or shift.To are within this shift range
            if (shift.From >= From && shift.From <= To) 
                return true;
                
            return shift.To >= From && shift.To <= To;
        }
    }
}