﻿namespace Planday.Shifts.API.Entities
{
    public class Employee
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Deleted { get; set; }

        public override bool Equals(object? obj)
        {
            if (obj is Employee employeeToCompare) 
                return employeeToCompare.Id == Id;

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}