# Planday
The solution contains three projects:

- **Planday.Shifts.API**: Where the API runs
- **Planday.Shifts.DbUpdater**: Scripts to initialize the SQL Database
- **Planday.Shifts.API.Tests**: unit tests

# How to run the API

Open the project and build the solution. The first thing you need to do is to configure the database. 

- Create an empty database in *MSSQL Server*. For example, **Planday**
- Open *Planday.Shifts.DbUpdater* and configure the connectionString in *App.config*.
- Configure the connectionString in the *Planday.Shifts.API* project. The file is *appSettings.json*
- Build the solution. Go to the *DbUpdater* bin folder and execute the console application. It will generate the tables.

Once with the schema created, run *Planday.Shifts.API*. By default, it will show the *Swagger* tool. Open _appSettings.json_ and copy the **JwtToken:Token** value. It is needed to authenticate the API calls.
